Source: qt6-declarative
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Patrick Franz <deltaone@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libgl-dev,
               liblttng-ust-dev [linux-any],
               libssl-dev,
               libvulkan-dev [linux-any],
               libxkbcommon-dev,
               mold,
               ninja-build,
               pkgconf,
               pkg-kde-tools,
               qt6-base-dev (>=6.8.2~),
#               qt6-declarative-dev:native <cross>,
               qt6-languageserver-dev (>=6.8.2~),
               qt6-location-dev,
               qt6-positioning-dev,
               qt6-shadertools-dev (>=6.8.2~),
               qt6-svg-dev,
               qt6-tools-dev,
Build-Depends-Indep: qt6-base-dev (>=6.8~) <!nodoc>,
                     qt6-base-doc (>=6.8~) <!nodoc>,
                     qt6-documentation-tools (>=6.8~) <!nodoc>,
Standards-Version: 4.6.2
Homepage: https://www.qt.io/developers/
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt6/qt6-declarative.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt6/qt6-declarative
Rules-Requires-Root: no

Package: qt6-declarative
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: qt6-declarative-abi (=6.8.2)
Breaks: libqt6labsanimation6,
        libqt6labsfolderlistmodel6,
        libqt6labsqmlmodels6,
        libqt6labssettings6,
        libqt6labssharedimage6,
        libqt6labswavefrontmesh6,
        libqt6qmlcore6,
        libqt6qmllocalstorage6,
        libqt6qmlworkerscript6,
        libqt6qmlxmllistmodel6,
        libqt6quickcontrols2impl6,
        libqt6quickdialogs2-6,
        libqt6quickdialogs2quickimpl6,
        libqt6quickdialogs2utils6,
        libqt6quicklayouts6,
        libqt6quickparticles6,
        libqt6quickshapes6
Replaces: libqt6labsanimation6,
          libqt6labsfolderlistmodel6,
          libqt6labsqmlmodels6,
          libqt6labssettings6,
          libqt6labssharedimage6,
          libqt6labswavefrontmesh6,
          libqt6qml6,
          libqt6qml6t64,
          libqt6qmlcompiler6,
          libqt6qmlcompiler6t64,
          libqt6qmlcore6,
          libqt6qmllocalstorage6,
          libqt6qmlmodels6,
          libqt6qmlmodels6t64,
          libqt6qmlworkerscript6,
          libqt6qmlxmllistmodel6,
          libqt6quick6,
          libqt6quick6t64,
          libqt6quickcontrols2impl6,
          libqt6quickcontrols2-6,
          libqt6quickcontrols2-6t64,
          libqt6quickdialogs2-6,
          libqt6quickdialogs2quickimpl6,
          libqt6quickdialogs2utils6,
          libqt6quicklayouts6,
          libqt6quickparticles6,
          libqt6quickshapes6,
          libqt6quicktemplates2-6,
          libqt6quicktemplates2-6t64,
          libqt6quicktest6,
          libqt6quickwidgets6,
          libqt6quickwidgets6t64,
Recommends: libgl1, libglx-mesa0
Suggests: qt6-qmltooling-plugins
Description: Qt 6 QML module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 The Qt QML module provides a framework for developing applications and
 libraries with the QML language. It defines and implements the language
 and engine infrastructure, and provides an API to enable application
 developers to extend the QML language with custom types and integrate
 QML code with JavaScript and C++.

Package: qt6-declarative-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: qt6-base-dev,
         qt6-declarative (= ${binary:Version}),
         qt6-languageserver-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Qt 6 declarative development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header development files for building some
 Qt 6 applications using Qt 6 declarative headers.

Package: qt6-declarative-doc
Architecture: all
Section: kde
X-Neon-MergedPackage: true
Depends: qt6-base-doc, qt6-declarative-dev, ${misc:Depends}, ${shlibs:Depends}
Description: Qt 6 declarative documentation files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the documentation files for helping to build some
 Qt 6 applications using Qt 6 declarative technology.

Package: libqt6qml6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6qml6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6qmlcompiler6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6qmlcompiler6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6qmlmodels6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6qmlmodels6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quick6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quick6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quickcontrols2-6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quickcontrols2-6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quicktemplates2-6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quicktemplates2-6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quicktest6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quickwidgets6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6quickwidgets6t64
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-qmllint-plugins
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-qmltooling-plugins
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qmltime
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-animation
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-folderlistmodel
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-platform
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-qmlmodels
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-settings
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-sharedimage
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qt-labs-wavefrontmesh
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtcore
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtqml
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtqml-base
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtqml-models
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtqml-workerscript
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtqml-xmllistmodel
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-controls
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-dialogs
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-effects
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-layouts
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-localstorage
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-nativestyle
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-particles
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-shapes
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-templates
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-tooling
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qtquick-window
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-qttest
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml-qt6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qmlscene-qt6
Architecture: all
Depends: qt6-declarative, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-declarative-dev-tools
Architecture: all
Depends: qt6-declarative-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-declarative-doc-dev
Architecture: all
Depends: qt6-declarative-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-declarative-doc-html
Architecture: all
Depends: qt6-declarative-doc, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-declarative-examples
Architecture: all
Depends: qt6-declarative-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-declarative-private-dev
Architecture: all
Depends: qt6-declarative-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
